import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switch';
import 'rxjs/add/operator/map';

import { GithubRepository } from './../model/GithubRepository';
import { GithubSearchService } from '../github-search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Output() results: EventEmitter<GithubRepository[]> = new EventEmitter<GithubRepository[]>();

  subject: Subject<string>;
  stream: Observable<string>;

  constructor(private github: GithubSearchService) { }

  ngOnInit() {
    this.subject = new Subject<string>();

    this.stream = this.subject.asObservable();

    this.stream
      .filter((text: string) => text.length > 1)
      .debounceTime(250)
      .map((query: string) => this.github.search(query))
      .switch()
      .subscribe(
        (repositories: GithubRepository[]) => {
          this.results.emit(repositories);
        },
        (err: any) => {
          console.log(err);
        }
      );
  }

  onKeyUp(event) {
    this.search(event.target.value);
  }

  search(query: string) {
    this.subject.next(query);
  }

}
