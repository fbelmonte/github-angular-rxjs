import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { GithubRepository } from './model/GithubRepository';

@Injectable()
export class GithubSearchService {

  private api = 'https://api.github.com';

  constructor(private http: Http) { }

  search(query: string): Observable<GithubRepository[]> {

    const params: string = [
      `q=${query}`,
      'sort=stars'
    ].join('&');

    const url = `${this.api}/search/repositories?${params}`;

    return this.http.get(url)
      .map((response: Response) => response.json().items )
      .map((items: GithubRepository[]) => {
        return items.map(item => {
          return new GithubRepository(item);
        });
      });

  }

}
