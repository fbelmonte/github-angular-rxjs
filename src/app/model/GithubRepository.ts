export class GithubRepository {

  id: number;
  name: string;
  url: string;
  description: string;
  forks: number;
  score: number;

  constructor(item: any) {
    this.id = item.id;
    this.name = item.name;
    this.url = item.url;
    this.description = item.description;
    this.forks = item.forks;
    this.score = item.score;
  }

}
