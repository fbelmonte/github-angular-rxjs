import { Component, OnInit, Input } from '@angular/core';
import { GithubRepository } from '../model/GithubRepository';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  @Input() results: GithubRepository[];

  constructor() { }

  ngOnInit() {
  }

}
