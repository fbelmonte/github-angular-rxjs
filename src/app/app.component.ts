import { Component } from '@angular/core';
import { GithubRepository } from './model/GithubRepository';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  results: GithubRepository[];

  constructor() {
    this.results = [];
  }

  renderResults(results: GithubRepository[]) {
    this.results = results;
  }

}
